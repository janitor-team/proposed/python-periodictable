python-periodictable (1.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version to 4.6.0 (no changes required).
  * Get lintian-brush to add in upstream metadata.

 -- Stuart Prescott <stuart@debian.org>  Mon, 18 Oct 2021 12:08:26 +1100

python-periodictable (1.5.3-1) unstable; urgency=medium

  * New upstream release.
   - refresh patches
   - avoid testing sphinx extensions
  * Update lintian override.
  * Move to debhelper-compat (= 13).
  * Remove obsolete image handling code for texi documentation.
  * Update Homepage.

 -- Stuart Prescott <stuart@debian.org>  Sat, 14 Nov 2020 15:29:43 +1100

python-periodictable (1.5.2-3) unstable; urgency=medium

  * png images no longer referenced in info file
    (generated by sphinx 2.4) Closes: #955076.

 -- Drew Parsons <dparsons@debian.org>  Thu, 16 Apr 2020 19:15:36 +0800

python-periodictable (1.5.2-2) unstable; urgency=medium

  * Only test with py3versions -s.
  * Update Standards-Version to 4.5.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Tue, 24 Mar 2020 01:54:04 +1100

python-periodictable (1.5.2-1) unstable; urgency=medium

  * New upstream release.

 -- Stuart Prescott <stuart@debian.org>  Sun, 01 Dec 2019 17:24:05 +1100

python-periodictable (1.5.1-2) unstable; urgency=medium

  * drop python-periodictable (remove Python2). Closes: #938017
  * drop ancient python version fields in debian/control
  * mark python-periodictable-doc as Multi-Arch: foreign
  * Standards-Version: 4.4.1

 -- Drew Parsons <dparsons@debian.org>  Sat, 12 Oct 2019 17:39:56 +0800

python-periodictable (1.5.1-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version to 4.4.0 (no changes required).
  * Update to debhelper-compat 12.

 -- Stuart Prescott <stuart@debian.org>  Wed, 25 Sep 2019 01:43:10 +1000

python-periodictable (1.5.0-8) unstable; urgency=medium

  [ Stuart Prescott ]
  * Fix typo in Suggests entry for documentation (Closes: #923744).

 -- Drew Parsons <dparsons@debian.org>  Wed, 26 Jun 2019 16:18:35 +0800

python-periodictable (1.5.0-7) unstable; urgency=medium

  * Replace sphinx directives that have been removed in matplotlib 3
    (Closes: #917749).
  * Use Python 3 for sphinx build.
  * Update Standards-Version to 4.3.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Sun, 30 Dec 2018 12:27:41 +1100

python-periodictable (1.5.0-6) unstable; urgency=medium

  * Add missing dependency on libjs-mathjax to the documentation package
    (Closes: #911070).
  * Update Standards-Version to 4.2.1 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Tue, 16 Oct 2018 11:46:06 +1100

python-periodictable (1.5.0-5) unstable; urgency=medium

  * Use pybuild.testfiles to copy around bits of the test suite.
  * Don't install rst files into useless "api" directory in module package.

 -- Stuart Prescott <stuart@debian.org>  Thu, 10 May 2018 23:54:03 +1000

python-periodictable (1.5.0-4) unstable; urgency=medium

  * Fix doctests to work with numpy 1.14.

 -- Stuart Prescott <stuart@debian.org>  Wed, 09 May 2018 00:58:39 +1000

python-periodictable (1.5.0-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.2 (no changes required).
  * Add Rules-Requires-Root: no.
  * Allow numerical fuzz in tests to prevent failures on i386.
  * Switch to debhelper 11.
    - move documentation to /usr/share/doc/python-periodictable as per
      Policy §12.3.

 -- Stuart Prescott <stuart@debian.org>  Sat, 30 Dec 2017 15:54:07 +1100

python-periodictable (1.5.0-2) unstable; urgency=medium

  [ Stuart Prescott ]
  * Add autopkgtest tests.
  * Enable build-time tests.
  * Fix package Section for module packages.
  * Make sphinx use packaged mathjax resources.

  [ Drew Parsons ]
  * periodictable/cromermann.py has a BSD-3-clause licence.

 -- Drew Parsons <dparsons@debian.org>  Tue, 14 Nov 2017 11:47:38 +0800

python-periodictable (1.5.0-1) unstable; urgency=medium

  * Team upload.
  * Initial release (Closes: #879833).

 -- Drew Parsons <dparsons@debian.org>  Sun, 29 Oct 2017 23:26:43 +0800
